Rails.application.routes.draw do

  get 'queries/index'

  root to: 'sheets#upload'
  
  resources :sheets, only: [] do
    collection do 
      get :upload
      post :import_service
      post :import_city 
      post :clear  
    end
  end

  resources :queries, only: :index do 
    collection do 
      post :fetch_lawyers
    end
  end
  
end
