class ServiceList < ActiveRecord::Base
  belongs_to :lawyer
  belongs_to :service
  has_many :payments 
end