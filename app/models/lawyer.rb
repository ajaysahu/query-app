class Lawyer < ActiveRecord::Base

  belongs_to :location  
 
  has_many :service_lists 
  has_many :services, through: :service_lists
  has_many :payments, through: :service_lists

  delegate :name, to: :location, prefix: true 
  delegate :id, to: :location, prefix: true 

  def self.joined_service_lawyer_location
    @joined_result ||= Service.joins([lawyers: :location], :payments)
  end

  
  def self.based_on_location_service(location_id, service_id)
    joined_service_lawyer_location
    .select('services.name AS SERVICE_NAME, lawyers.code AS LAW_CODE,
      lawyers.name AS LAWYER_NAME,payments.charge as AMOUNT,
      locations.name AS LOCATION_NAME')
    .where('locations.id = ? AND services.id = ?', location_id, service_id).group('lawyers.id')
  end

end
