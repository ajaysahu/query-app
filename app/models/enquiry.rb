# created this class, to verify the query functions. 
class Enquiry

  attr_accessor :result 

  def self.joined_location_law_services
    @result ||= Location.joins([lawyers: :services])
  end

  def self.query_1(city_name= 'Chennai', service_name= 'Divorce') 
    city_name.downcase!.strip!
    service_name.downcase!.strip!
    lawyers_on_subject
    @result ||= joined_location_law_services 
    .where('locations.name = ? AND services.name = ? ', city_name, service_name)
  end

  def lawyers_on_subject(queried_service)
    @cool ||= Enquiry.joined_location_law_services
    @cool.where(services: { name: queried_service })
  end 

  def lawyers_on_location_n_service(location, service)
    @result ||= joined_location_law_services
    .where('locations.name = ? AND services.name = ? ', location, service)
  end


end