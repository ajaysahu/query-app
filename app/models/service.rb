class Service < ActiveRecord::Base
  has_many :service_lists
  has_many :payments, through: :service_lists
  has_many :lawyers, through: :service_lists
  
  scope :pluck_services, -> { select('id, name').order(:name) }
end
