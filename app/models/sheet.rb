class Sheet 
  @location_file_path
  @service_file_path 

  class << self
    attr_accessor :location_file_path 
    attr_accessor :service_file_path  
  end
 
  def self.upload_location_and_lawyers
    return false unless location_and_lawyer_blank? 
    get_locations
    get_lawyers
    link_location_lawyer
    true
  end

  def self.location_and_lawyer_blank?
    Lawyer.all.blank? && Location.all.blank?
  end

  def self.upload_services
    return false unless Service.all.blank?
    get_services
    link_payment_lawyer_service
    true
  end 

  def self.raw_location_lawyers
    @raw ||= Roo::CSV.new(location_file_path)
  end

  def self.raw_services
    @raw ||= Roo::CSV.new(service_file_path)
  end

  def self.load_locations
    locations = []
    raw_location_lawyers.each do |code, name, experience, location, rating|
      location.downcase!
      location.strip!
      locations.push location
    end

    locations.shift
    locations.uniq!
    locations.sort!
  end
 
  def self.load_lawyers
    lawyers = []
    raw_location_lawyers.each do |code, name, experience, location, rating|
      lawyer  = { 
        code: code.try(:strip), 
        name: name.try(:strip).try(:downcase),
        experience: experience.try(:to_i), 
        rating: rating.try(:to_f) 
      }
      lawyers.push(lawyer)
    end

    lawyers.shift
    lawyers
  end

  def self.load_services
    services = []
    raw_services.each do |law_code, service_code, service_name, charge|
      service_code.try(:strip!)
      service_name.try(:strip!).try(:downcase!)

      service = {
        code: service_code,
        name: service_name
      }
      services.push(service)
    end

    puts services.inspect 
    puts "before shift size is #{services.size}"
    services.shift
    services.uniq! 
    puts "after uniq! size is #{services.size}"
    services
  end
 

  def self.get_locations 
    load_locations.map { |location|
      Location.create(name: location)
    }
  end

  def self.get_lawyers
    load_lawyers.map { |lawyer|
      Lawyer.create(lawyer)
    }
  end

  def self.get_services 
    load_services.map { |service| 
      Service.create(service)
    }
  end

  def self.link_location_lawyer
    raw_location_lawyers.each do |code, name, experience, location, rating|
      lawyer = Lawyer.where(code: code).first 
      location = Location.where(name: location).first 
      lawyer.update_attributes(location_id: location.id) if location.present? 
    end
  end

  def self.link_payment_lawyer_service
    raw_services.each do |law_code, service_code, service_name, charge|
      service = Service.where(code: service_code.strip).first 
      lawyer = Lawyer.where(code: law_code.strip).first 
      if (service.present? && lawyer.present?)
        s = ServiceList.create(lawyer_id: lawyer.id, service_id: service.id)
        Payment.create(service_list_id: s.id, charge: charge.try(:to_f)) 
      end
    end
  end

  def self.clear_all
    Lawyer.delete_all
    Location.delete_all 
    Payment.delete_all
    Service.delete_all 
    ServiceList.delete_all 
  end
end
