
class Location < ActiveRecord::Base
  has_many :lawyers
  has_many :services, through: :lawyers

  scope :pluck_locations, -> { select('id, name').order(:name) }

end