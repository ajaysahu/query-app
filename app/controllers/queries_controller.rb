class QueriesController < ApplicationController
  before_action :get_services, only: :index
  before_action :get_locations, only: :index 

  def index
  end

  def fetch_lawyers
    location_id = params[:location_id] 
    service_id = params[:service_id]
    @eligible_lawyers = Lawyer.based_on_location_service(location_id, service_id)
  end


  private
  def get_services
    @service_list = Service.pluck_services
    if @service_list.blank?
      flash[:error] = "Please import records before you query."
      redirect_to root_path 
    end

  end

  def get_locations
    @location_list = Location.pluck_locations
    if @location_list.blank? 
      flash[:error] = "Please import records before you query."
      redirect_to root_path
    end
  end

  
end
