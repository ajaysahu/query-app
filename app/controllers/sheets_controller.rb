class SheetsController < ApplicationController
  
  before_action :get_file, only: [:import_city, :import_service]

  def upload
  end

  def import_city 

    if @file.present? 
      Sheet.location_file_path = @file 
      @status = Sheet.upload_location_and_lawyers
    end
  end

  def import_service
    if @file.present? 
      Sheet.service_file_path = @file 
      @status = Sheet.upload_services
    end
  end

  def clear
    Sheet.clear_all
    flash[:success] = "Cleared all data."
    redirect_to root_path
  end


  private
  def get_file
    @file = params[:file].path if params[:file].present? 
  end


end