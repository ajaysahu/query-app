class CreateServiceLists < ActiveRecord::Migration
  def change
    create_table :service_lists do |t|
      t.integer :lawyer_id, index: true
      t.integer :service_id, index: true
    end
  end
end