class CreateLawyers < ActiveRecord::Migration
  def change
    create_table :lawyers do |t|
      t.string :code, index: true 
      t.string :name
      t.integer :experience
      t.decimal :rating, precision: 8, scale: 2
      t.references :location, index: true

      t.timestamps
    end
  end
end