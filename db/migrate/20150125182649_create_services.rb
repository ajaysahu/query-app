class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :code, index: true 
      t.string :name, index: true 
      t.timestamps
    end
  end
end