class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :service_list, index: true
      t.decimal :charge, precision: 8, scale: 2

      t.timestamps
    end
  end
end
